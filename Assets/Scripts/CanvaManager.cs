﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CanvaManager : MonoBehaviour
{
    [SerializeField] private Text interact;
    [SerializeField] private Canvas canvas;
    [SerializeField] private GameObject BoardGrid;
    [SerializeField] private Sprite scrollPic;
    [SerializeField] private Image menuBoard;
    [SerializeField] private Text menuTxt;
    [SerializeField] private Button btnAdd;
    [SerializeField] private Button CloseMenuBTN;

    [SerializeField] private Image scrollView;
    [SerializeField] private Text scrollViewTitle;
    [SerializeField] private Text scrollViewBody;
    [SerializeField] private Button scrollViewDelete;
    [SerializeField] private Button scrollViewClose;

    [SerializeField] private Image editScroll;
    [SerializeField] private InputField title;
    [SerializeField] private InputField body;
    [SerializeField] private Button submit;
    [SerializeField] private Button cancel;

    private List<string> titles = new List<string>();
    private List<string> bodies = new List<string>();
    private List<GameObject> scrolls = new List<GameObject>();

    private static bool viewOn;
    private static int sNum;
    public static bool isEditOn;
    private static int thisScroll;
    private int totalScroll;
    private string townSize;

    void Start()
    {
        sNum = 0;
        thisScroll = 0;
        townSize = "large"; //------change here based on town size

        positAdjust();//sizes board

        if (townSize.Equals("large"))
        {
            menuBoard.GetComponent<RectTransform>().sizeDelta = new Vector2(700, 500);
            menuTxt.GetComponent<RectTransform>().position.Set(0, 210, 0);
            CloseMenuBTN.GetComponent<RectTransform>().position.Set(320, 220, 0);
        }
        else if (townSize.Equals("medium"))
        {
            menuBoard.GetComponent<RectTransform>().sizeDelta = new Vector2(700, 400);
            menuTxt.GetComponent<RectTransform>().position.Set(0, 200, 0);
            CloseMenuBTN.GetComponent<RectTransform>().position.Set(320, 180, 0);
        }
        else if (townSize.Equals("small"))
        {
            menuBoard.GetComponent<RectTransform>().sizeDelta = new Vector2(400, 400);
            menuTxt.GetComponent<RectTransform>().position.Set(0, 180, 0);
            CloseMenuBTN.GetComponent<RectTransform>().position.Set(180, 180, 0);
        }
    }

    void Update()
    {   //manage menu board view
        if (menuBoard.enabled) { TextImg.isImgOn = true; }
        if (TextImg.isImgOn) { interact.enabled = false; }
        if (Input.GetKeyDown(KeyCode.F) && !TextImg.isImgOn && TextImg.isCol)
        {
            menuBoard.enabled = true;
            menuTxt.enabled = true;
            TextImg.isImgOn = true;
            CloseMenuBTN.gameObject.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.F) && TextImg.isImgOn || isEditOn)
        {
            menuBoard.enabled = false;
            menuTxt.enabled = false;
            CloseMenuBTN.gameObject.SetActive(false);
            TextImg.isImgOn = false;
        }
        if (TextImg.isCol && !TextImg.isImgOn && !isEditOn && !viewOn) { interact.enabled = true; }
        // manage selected scroll preview
        if (!viewOn)
        {
            ViewScroll(false);
            scrollViewTitle.text = null;
            scrollViewBody.text = null;
        }
        if (!isEditOn) { ScrollEdit(false); }

        // manage scrolls on menu
        if (!menuBoard.enabled)
        {
            if (sNum > 0)
            {
                foreach (GameObject btn in scrolls)
                {
                    btn.gameObject.SetActive(false);
                }
            }
            btnAdd.gameObject.SetActive(false);
            CloseMenuBTN.gameObject.SetActive(false);
        }
        else if (menuBoard.enabled)
        {
            for (int i = 0; i < scrolls.Count; i++)
            {
                scrolls[i].SetActive(true);
            }
            btnAdd.gameObject.SetActive(true);
            CloseMenuBTN.gameObject.SetActive(true);
        }
    }

    public void CloseMenu()
    {
        TextImg.isImgOn = false;
        menuBoard.enabled = false;
        //menuTxt.enabled = false;
        CloseMenuBTN.gameObject.SetActive(false);
    }

    public void Accept() //accept button for editScroll
    {
        if (sNum < totalScroll)
        {
            titles.Add(title.text);
            bodies.Add(body.text);

            GameObject btn = new GameObject();
            btn.transform.parent = BoardGrid.transform;
            Button b1 = btn.AddComponent<Button>();
            btn.AddComponent<RectTransform>();
            btn.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 90);
            btn.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 100);
            btn.GetComponent<Button>().onClick.AddListener(ScrollClick);
            btn.AddComponent<Image>();
            btn.GetComponent<Image>().sprite = scrollPic;
            scrolls.Add(btn);
        }
        if (sNum < totalScroll)
        {
            sNum += 1;
        }
        //-----clear off edit scroll items
        title.text = null;
        body.text = null;
        ScrollEdit(false);
        isEditOn = false;

        //--------return to menu 

        if (sNum < totalScroll)
        {
            btnAdd.gameObject.SetActive(true);
            menuTxt.enabled = true;
        }
        else
        {
            btnAdd.gameObject.SetActive(false);
            menuTxt.enabled = true;
        }
        menuBoard.enabled = true;
    }
    public void setMsg() //"add message" button function
    {
        if (sNum < totalScroll)
        {
            ScrollEdit(true);
            isEditOn = true;
        }
        else
        {
            isEditOn = false;
        }
    }
    public void Cancel() // cancel edit view
    {
        title.text = null;
        body.text = null;
        ScrollEdit(false);
        isEditOn = false;

        if (sNum < totalScroll)
        {
            btnAdd.gameObject.SetActive(true);
            menuTxt.enabled = true;
        }
        menuBoard.enabled = true;
    }
    public void Close() //close view scroll
    {
        viewOn = false;
        thisScroll = 0;

        menuBoard.enabled = true;
        menuTxt.enabled = true;
    }
    public void Delete() //delete the selected scroll
    {
        titles.RemoveAt(thisScroll);
        bodies.RemoveAt(thisScroll);
        Destroy(scrolls[thisScroll]);
        scrolls.RemoveAt(thisScroll);


        List<GameObject> temp = new List<GameObject>();
        foreach (GameObject o in scrolls)
        {
            if (o != null)
            {
                temp.Add(o);
            }
        }
        scrolls = temp;

        if (scrolls.Count <= 5)
        {
            sNum -= 1;
        }

        viewOn = false;
        menuBoard.enabled = true;
        menuTxt.enabled = true;
    }
    public void ScrollClick() //view selected scroll
    {
        viewOn = true;
        thisScroll = scrolls.IndexOf(EventSystem.current.currentSelectedGameObject);

        scrollViewTitle.text = titles[thisScroll];
        scrollViewBody.text = bodies[thisScroll];

        ViewScroll(true);

        menuBoard.enabled = false;
        menuTxt.enabled = false;
    }
    void ScrollEdit(bool enabled)
    {
        editScroll.gameObject.SetActive(enabled);
    }
    void ViewScroll(bool enabled)
    {
        scrollView.gameObject.SetActive(enabled);
    }
    void positAdjust()
    {
        GridLayoutGroup glg = BoardGrid.GetComponent<GridLayoutGroup>();
        if (townSize.Equals("large"))
        {
            totalScroll = 15;
            BoardGrid.GetComponent<RectTransform>().sizeDelta = new Vector2(600, 330);

        }
        else if (townSize.Equals("medium"))
        {
            totalScroll = 10;
            BoardGrid.GetComponent<RectTransform>().sizeDelta = new Vector2(600, 330);
        }
        else
        {
            totalScroll = 5;
            BoardGrid.GetComponent<RectTransform>().sizeDelta = new Vector2(350, 330);
        }
    }
}