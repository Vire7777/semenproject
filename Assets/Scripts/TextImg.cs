﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextImg : MonoBehaviour
{
    
    [SerializeField] private Text text;
    [SerializeField] private GameObject Cube;
    [SerializeField] private GameObject Sphere;
    [SerializeField] private Image menuBoard;
    [SerializeField] private Text messageBoard;
    public static bool isImgOn;
    public static bool isCol;
    //---------------------------menu not visible-------------------
    void Start()
    {
        text.enabled = false;
        menuBoard.enabled = false;
        messageBoard.enabled = false;
        isImgOn = false;
        isCol = false;
    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player")) 
        {
            text.text = "INTERACT";
            text.enabled = true;
            isCol = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            text.enabled = false;
            menuBoard.enabled = false;
            isCol = false;
            isImgOn = false;
        }
    }
}